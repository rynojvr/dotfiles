#!/usr/bin/env python3

import argparse
import os

logfile = open('/tmp/log', 'a')

vpn_files_dir = os.getenv('HOME') + '/.vpn'
vpn_conf_dir = '/etc/openvpn/'
vpn_display_names = {
        'rynojvr-htb-dante': 'HTB: Dante',
        'rynojvr-htb-ra': 'HTB: Release Arena',
        'rynojvr-htb': 'HackTheBox',
        'rynojvr-thm': 'TryHackMe',
        'rynojvr-thm-throwback': 'THM: Throwback',
        'rynojvr-thm-wreath': 'THM: Wreath'
        }
current_vpn = ''
def read_current_vpn():
    global current_vpn
    try:
        with open(f"{vpn_files_dir}/current", 'r') as f:
            current_vpn = f.read()
    except Exception:
        current_vpn = 'rynojvr-htb'
read_current_vpn()

def syscmd(cmd):
    result = ''
    try:
        result = os.popen(cmd).read().strip()
    except:
        result = 'ERROR'
    return result


def vpn_running():
    return syscmd(f"sudo /usr/sbin/service openvpn@{current_vpn} status | grep running")

def vpn_closing():
    return syscmd(f"sudo /usr/sbin/service openvpn@{current_vpn} status | grep deactivating")

def vpn_connected():
    return syscmd('/usr/sbin/ifconfig | grep tun')

def vpn_connecting():
    return vpn_running() and not vpn_connected()

def vpn_ip():
    if vpn_connected():
        return syscmd('/usr/sbin/ifconfig | grep -A 1 tun | grep inet | awk \'{print $2}\'')
    return ''

def vpn_report():
    pass

def vpn_disconnect():
    syscmd(f"sudo /usr/sbin/service openvpn@{current_vpn} stop")
    syscmd("sleep 2")


def vpn_connect():
    logfile.write("Starting vpn: " + current_vpn + "\n")
    print(f"sudo /usr/sbin/service openvpn@{current_vpn} start")
    syscmd(f"sudo /usr/sbin/service openvpn@{current_vpn} start")
    print("Connect done")


def vpn_toggle_connection():
    if vpn_connected():
        vpn_disconnect()
    else:
        vpn_connect()


def vpn_location_menu():
    # TODO: Not ugly lines like this
    options = [x[2] for x in os.walk(vpn_conf_dir)][0]
    options = [x.removesuffix(".conf") for x in options if x.endswith(".conf")]
    options = [vpn_display_names[x] or x for x in options]
    opt_list = "|".join(options)

    choice = syscmd(f"echo '{opt_list}' | rofi -location 3 -xoffset -50 -yoffset 50 -columns 1 -width 10 -hide-scrollbar -line-padding 4 -padding 20 -lines 6 -sep '|' -dmenu -i -p 'Choose VPN'")
    for file_name in vpn_display_names: 
        if vpn_display_names[file_name] == choice:
            global current_vpn
            vpn_disconnect()
            current_vpn = file_name
            logfile.write("Selected vpn: " + current_vpn + "\n")
            print("Selected vpn: " + current_vpn + "\n")
            with open(f"{vpn_files_dir}/current", 'w') as f:
                f.write(current_vpn)
            print("disconnected vpn; waiting...")
            print("print connecting")
            vpn_connect()
            print("Connected?")

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--toggle_connection", default=False, action="store_true")
    parser.add_argument("--location_menu", default=False, action="store_true")
    parser.add_argument("--ip_address", default=False, action="store_true")
    args = parser.parse_args()
    return args

if __name__ == "__main__":
    args = parse_args()
    #print("VPN Module")

    if vpn_closing():
        print("VPN Closing...")
    elif vpn_connected():
        current_vpn_name = current_vpn
        try:
            current_vpn_name = vpn_display_names[current_vpn]
        except Exception:
            pass
        print(f"{current_vpn_name}: {vpn_ip()}")
    elif vpn_connecting():
        print("VPN Connecting...")
    else:
        print("Not Connected")

    if args.toggle_connection: 
        vpn_toggle_connection()

    if args.location_menu:
        vpn_location_menu()


