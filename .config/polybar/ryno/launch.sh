#!/usr/bin/env bash

# Add this script to your wm startup file.

DIR="$HOME/.config/polybar/ryno"

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch the bar
polybar -q main -c "$DIR"/config.ini &


#polybar -q top -c "$HOME/.config/polybar/forest/preview.ini" &
#polybar -q mid -c "$HOME/.config/polybar/forest/preview.ini" &
#polybar -q bottom -c "$HOME/.config/polybar/forest/preview.ini" &
